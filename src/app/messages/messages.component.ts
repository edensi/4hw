import { Component, OnInit } from '@angular/core';
import { MessagesService } from './../messages.service';

@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  //messages=['Message1','Message2','Message3','Message4'];
  messages;
  constructor() { 
    let service = new MessagesService();
    this.messages = service.getMessages();

  }

  ngOnInit() {
  }

}